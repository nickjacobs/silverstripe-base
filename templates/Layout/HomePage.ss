<section>
	<div class="container">
		<div class="row typography">

			<div class="four columns">
				<h1 class="pagetitle">$Title</h1>
			</div>

			<div class="eight columns">
				
			
				<div class="page-summary">
				$PageSummary
				</div>


				$Content

			</div>

		</div>
	</div><!-- .container -->
</section>
